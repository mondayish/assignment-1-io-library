section .text


; Принимает код возврата и завершает текущий процесс
exit:
    mov rax, 60
    syscall

; Принимает указатель на нуль-терминированную строку, возвращает её длину
string_length:
      .loop:
          xor rax, rax

      .count:
          test byte [rdi + rax], 0xff
          je .end
          inc rax
          jmp .count

      .end:
          ret

; Принимает указатель на нуль-терминированную строку, выводит её в stdout
print_string:
    push rdi
    call string_length
    pop rsi
    mov  rdx, rax
    mov  rax, 1
    mov  rdi, 1
    syscall
    ret

; Принимает код символа и выводит его в stdout
print_char:
    push rdi
    mov rsi, rsp
    pop rax

    mov rax, 1
    mov rdi, 1
    mov rdx, 1
    syscall
    ret

; Переводит строку (выводит символ с кодом 0xA)
print_newline:
    mov rdi, 0xA
    jmp print_char

; Выводит беззнаковое 8-байтовое число в десятичном формате
; Совет: выделите место в стеке и храните там результаты деления
; Не забудьте перевести цифры в их ASCII коды.
print_uint:
    xor rax, rax
    mov r9, rsp     ; сохраним rsp
    mov r8, 10      ; для перевода в десятичный формат
    push 0          ; конец строки
    mov rax, rdi

    .loop:
        dec rsp
        xor rdx, rdx
        div r8
        add rdx, '0'      ; переводим цифру в ASCII код
        mov byte[rsp], dl ; кладем цифру на стек
        cmp rax, 0
        je .end
        jmp .loop

    .end:
        mov rdi, rsp
        call print_string
        mov rsp, r9
    ret

; Выводит знаковое 8-байтовое число в десятичном формате
print_int:
    xor rax, rax
    cmp rdi, 0
    jns .uint

    .to_uint:
        mov r8, rdi
        neg r8          ; делаем положительным
        mov rdi, '-'    ; выводим для отрицательного числа
        call print_char
        mov rdi, r8

    .uint:
        call print_uint
        ret

; Принимает два указателя на нуль-терминированные строки, возвращает 1 если они равны, 0 иначе
string_equals:
    .loop:
        mov r8b, byte[rdi]
        mov r9b, byte[rsi]
        cmp r8b, r9b
        jne .not_equals
        inc rdi
        inc rsi
        test r9b, r9b
        jnz .loop

    .equals:
        xor rax, rax
        inc rax
        ret

    .not_equals:
        xor rax, rax
        ret

; Читает один символ из stdin и возвращает его. Возвращает 0 если достигнут конец потока
read_char:
    push 0       ; в случае конца потока просто 0 будет
    mov rsi, rsp ; считаем символ на вершину стека
    xor rdi, rdi
    xor rax, rax
    mov rdx, 1

    syscall
    pop rax
    ret

; Принимает: адрес начала буфера, размер буфера
; Читает в буфер слово из stdin, пропуская пробельные символы в начале, .
; Пробельные символы это пробел 0x20, табуляция 0x9 и перевод строки 0xA.
; Останавливается и возвращает 0 если слово слишком большое для буфера
; При успехе возвращает адрес буфера в rax, длину слова в rdx.
; При неудаче возвращает 0 в rax
; Эта функция должна дописывать к слову нуль-терминатор
read_word:
    mov r9, rdi   ; сохраняем адрес буффера
    mov r10, rsi  ; сохраняем размер буффера
    xor r8, r8    ; счетчик символов

    .skip_spaces:
        call read_char
        cmp rax, 0x0
        je .error_end
        cmp rax, 0x20
        je .skip_spaces
        cmp rax, 0x9
        je .skip_spaces
        cmp rax, 0xA
        je .skip_spaces
        mov byte[r8 + r9], al   ; сохраняем символ с учетом переноса
        inc r8

    .loop:
        cmp r8, r10
        je .error_end           ; если счетчик достиг размера буффера, то ошибка
        call read_char
        cmp rax, 0x0
        je .success_end
        cmp rax, 0x20
        je .success_end         ; если пробел, значит слово закончилось
        mov byte[r8 + r9], al
        inc r8
        jmp .loop

    .error_end:
        xor rax, rax
        xor rdx, rdx
        ret

    .success_end:
        mov rdx, r8
        add r8, r9
        mov byte[r8], 0x0
        mov rax, r9
        ret


; Принимает указатель на строку, пытается
; прочитать из её начала беззнаковое число.
; Возвращает в rax: число, rdx : его длину в символах
; rdx = 0 если число прочитать не удалось
parse_uint:
    xor rax, rax
    xor rcx, rcx ; длина в символах
    mov r8, 10   ; для сдвига в rax

    .loop:
        xor r9, r9
        mov r9b, [rdi + rcx]
        cmp r9, 0x0
        je .success_end
        sub r9, 0x30     ; вычитаем код '0'
        cmp r9, 0x0
        jb .success_end
        cmp r9, 0x9
        ja .success_end
        mul r8
        add rax, r9
        inc rcx
        jmp .loop

    .success_end:
        mov rdx, rcx
        ret



; Принимает указатель на строку, пытается
; прочитать из её начала знаковое число.
; Если есть знак, пробелы между ним и числом не разрешены.
; Возвращает в rax: число, rdx : его длину в символах (включая знак, если он был)
; rdx = 0 если число прочитать не удалось
parse_int:
    xor r8, r8
    mov r8b, [rdi]   ; проверим первый символ и в зависимости от него выберем ветку
    cmp r8, '-'
    je .negative

    .positive:
        call parse_uint
        ret

    .negative:
        inc rdi
        call parse_uint
        inc rdx
        neg rax
        ret

; Принимает указатель на строку, указатель на буфер и длину буфера
; Копирует строку в буфер
; Возвращает длину строки если она умещается в буфер, иначе 0
string_copy:
    call string_length
    cmp rax, rdx
    ja .error_end
    xor rcx, rcx    ; счетчик длины строки
    xor r8, r8

    .loop:
        mov r8, [rdi + rcx]
        mov [rsi + rcx], r8
        cmp rcx, rax
        je .success_end
        inc rcx
        jmp .loop

    .error_end:
        xor rax, rax
        ret

    .success_end:
        mov rax, rcx
        ret